import React from 'react'
import ReactDOM from 'react-dom'
import {Button, InputNumber, Select, Checkbox} from 'antd';
import "antd/dist/antd.css";
import "./style.scss"

const {Option} = Select;

class Diashow extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            time: 60,
            pics: 5,
            isDiashow: false,
            pic: 0,
            useTimer: true,
            timerRunning: false,
            timerSec: -1,
            timerMin: 0,
        }
    }

    //sets the picture counter up or leave show mode when last pic
    nextPic() {
        let newPic = this.state.pic + 1;
        if (newPic > this.state.pics) {
            this.setState({isDiashow: false, pic: 0, timerSec: -1, timerRunning: false})
        } else {
            this.setState({pic: newPic, timerRunning: true})
            this.resetTimer();
        }
    }

    //when mounted, setup tick for timer
    componentDidMount() {
        //setup the timer
        this.timerID = setInterval(
            () => this.tick(),
            1000
        )
    }

    //reset timer to time from input
    resetTimer() {
        let sec = this.state.time % 60;
        let min = (this.state.time - sec) / 60
        this.setState({
            timerSec: sec + 1,
            timerMin: min,
        })
    }

    //sets the timer and the states to start the diashow
    startDiashow() {
        this.resetTimer();
        this.setState({
            isDiashow: true,
            pic: 1,
            timerRunning: true,
        })
    }

    //every tick, reduce the second counter, or reset the timer if seconds are less then 0
    tick() {
        if (this.state.useTimer && this.state.timerRunning) {
            let min = this.state.timerMin;
            let sec = this.state.timerSec;
            //set up the timer, e.g. when new pic is set
            if (sec < 0) {
                this.resetTimer()
            } else {
                //reduce the time and switch to next picture if necessary
                sec -= 1;
                if (sec < 0) {
                    sec = 59;
                    min -= 1;
                }
                if (min < 0) {
                    this.nextPic()
                } else {
                    this.setState({
                        timerSec: sec,
                        timerMin: min,
                    })
                }
            }
        }

    }

    //remove the timer, when unmounted
    componentWillUnmount() {
        clearInterval(this.timerID);
    }

    getImage() {

        const {innerWidth: width, innerHeight: height} = window;
        return "https://picsum.photos/" + Math.round(width) + "/" + Math.round(height * 0.9) + "?random=" + this.state.pic;
    }

    //padd a number to two digits (so with leading 0)
    twoDigits = (num) => String(num).padStart(2, '0')


    render() {
        if (this.state.isDiashow) {
            return (
                <div class="image-container">
                    <img src={this.getImage()}/>
                    <div id="overlay">
                        <p>Picture {this.state.pic}/{this.state.pics}</p>
                        {this.state.useTimer &&
                        <p>{this.state.timerMin}:{this.twoDigits(this.state.timerSec)}</p>
                        }
                        <Button onClick={() => this.nextPic()}>Next</Button>
                    </div>
                </div>
            )
        } else {
            return (
                <div class="flex">
                    <div class="item">
                        <Checkbox class="white-text" onChange={(e) => {
                            this.setState({useTimer: e.target.checked})
                        }} defaultChecked autoComplete={false}>Timer?</Checkbox>

                        {this.state.useTimer &&
                        <Select defaultValue={this.state.time} onChange={(value) => this.setState({time: value})}>
                            <Option value={15}>15s per Picture</Option>
                            <Option value={30}>30s per Picture</Option>
                            <Option value={60}>60s per Picture</Option>
                            <Option value={90}>90s per Picture</Option>
                            <Option value={120}>120s per Picture</Option>
                        </Select>
                        }
                    </div>
                    <div class="item">
                        <p>Number of Pictures:
                        <InputNumber min={1} max={100} defaultValue={this.state.pics} onChange={(v) => this.setState({pics: v})}/>
                            </p></div>
                    <div class="item">
                        <Button onClick={() => this.startDiashow()}>Start!</Button>
                    </div>
                </div>
            )
        }
    }
}

export function App() {
    return (
        <div class="background">
            <h1 class="flex white-text">Powerpoint Picture Karaoke</h1>
            <Diashow/>
        </div>
    );
}