# Powerpoint Karaoke

This is a simple implementation of Powerpoint Karaoke with random images using React. A compiled version can be found [here](https://yelteen.gitlab.io/powerpoint-karaoke)

## Installation
The website is based on React and [parcel.js](https://parceljs.org/). For development, just use ```yarn``` for installation and then ```yarn start src/index.html``` to start a local server under http://localhost:1234

## Authors and acknowledgment
This is just a simple project by myself to get used to React and to create a webpage, which I was looking for some time ago. 

## License
This project is licenced under the [MIT License](https://opensource.org/licenses/MIT)


